package org.Batili.Exo1;

public class Compteur {
	int compteur=0;
	Object lock =new Object();
	public int getCompteur() {
		return compteur;
		
	}
	public void setCompteur(int compteur) {
		this.compteur=compteur;
		
	}
	public void increment() {
		synchronized (lock) {
			compteur++;
			
		}
		
	}

}
