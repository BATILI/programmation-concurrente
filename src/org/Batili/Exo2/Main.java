package org.Batili.Exo2;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Warehouse wharehouse= new Warehouse(10);
		Runnable add=()->{
		wharehouse.add();
		};
		
		Runnable remove=()->{
		wharehouse.remove();
		};
		ExecutorService myexecutor= Executors.newFixedThreadPool(5);
		for (int i = 0; i < 20; i++) {
			myexecutor.submit(add);
		}
		for (int i = 0; i < 15; i++) {
			myexecutor.submit(remove);
			
		}
		
		Thread.sleep(1_000);
		myexecutor.shutdown();
		System.out.println("Reste="+wharehouse.getAmount());
	}

}
