package org.Batili.Exo2;

public class Warehouse {
	int capacity;
	int amount;
	private final Object key=new Object();
	public Warehouse(int capacity) {
		this.capacity=capacity;
	
	}
	public void setAmount(int amount) {
		this.amount=amount;
		
	}
	public void add() {
		synchronized (key) {
			while (this.amount==this.capacity) {
				try {
					key.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			this.amount++;
		}
	}
public void remove() {
	synchronized (key) {
		this.amount--;
		key.notifyAll();
	}
	
}
public  int getCapacity() {
	return capacity;
	
}
public  int getAmount() {
	return amount;
}
}
