package org.Batili.Exo3;
public class Product {
	private String name;
	private Integer  prix;
	private long id;
	
	
	public Product(long id,String name,Integer prix)
	{
		this.id=id;
		this.name=name;
		this.prix=prix;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getPrix() {
		return prix;
	}


	public void setPrix(int prix) {
		this.prix = prix;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}
	
	
	

}
