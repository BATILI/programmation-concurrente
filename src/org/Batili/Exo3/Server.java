package org.Batili.Exo3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) throws IOException {

    	ConcurrentHashMap<String,Integer>  Magasin= new ConcurrentHashMap<>();
    	
    	Map<Long,Product>  MapProduct= new HashMap<>();  // for Question 8
    	
		ServerSocket server = new ServerSocket(8080);

        while (true) {
            System.out.println("Listening to request");
            Socket socket = server.accept();
            System.out.println("Accepting request");
            
            
        Callable<String> task=()->
        {
        	
     

            InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            OutputStream outputStream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));

            String order = reader.readLine();
            while (order != null) {
                if (order.startsWith("GET")) {
                    writer.printf("Received GET order : %s\n", order);
                    writer.flush();
                    System.out.printf("Received GET order : %s\n", order);
                    
                    
                } else if (order.equals("bye")) {
                    System.out.printf("Closing connection\n");
                    socket.close();
                    
                    
                }else if(order.startsWith("PUT"))
                {
                	  writer.printf("Received PUT order : %s\n", order);
                	  writer.flush();
                	  System.out.printf("Received PUT order : %s\n", order);
                	  
                	  String [] machaine= order.split(" ");
                	  Magasin.put(machaine[1], Integer.parseInt(machaine[2]));
                	  
                	  
                	  
                }else if(order.startsWith("LIST"))
                {
              	  writer.printf("Received LIST order : %s\n", order);
              	  writer.flush();
              	  System.out.printf("Received LIST order : %s\n", order);
              	  Magasin.forEach((nom,prix)->System.out.println(nom+ "--->"+prix));
               
                
                }else if(order.startsWith("BUY"))
                {
                	 writer.printf("Received BUY order : %s\n", order);
                 	  writer.flush();
                 	  System.out.printf("Received BUY order : %s\n", order);
                 	
                 	  
                 	  Magasin.remove(order.split(" ")[0]);
                 		  
                }
              	 
                
                
                else if(order.startsWith("CREATE"))
                {
                	 writer.printf("Received CREATE order : %s\n", order);
                	  writer.flush();
                	  System.out.printf("Received CREATE order : %s\n", order);
                	  
                	  if(!order.split(" ")[1].isEmpty())
                	  {
                	  Product myproduct= new  Product(Long.parseLong(order.split(" ")[0]),order.split(" ")[1],Integer.parseInt(order.split(" ")[2]));
                	
                	  System.out.println("CREATED" +myproduct.getId()+" ");
            
                	  MapProduct.put(myproduct.getId(),myproduct) ;
                	  }
                	  
                }
                
           
                
                
               
                order = reader.readLine();
            }
			return null;

        };
        
        ExecutorService myServerService= Executors.newFixedThreadPool(4);
        
        myServerService.submit(task);
        }
        
    }
}